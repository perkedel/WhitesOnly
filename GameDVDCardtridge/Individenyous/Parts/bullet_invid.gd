extends RigidBody2D

@export var initVelocity:float = 100
@export var wayLeft:bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	#linear_velocity.x = initVelocity * (-1 if wayLeft else 1)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func shootWay(toLeft:bool = false):
	wayLeft = toLeft
	linear_velocity.x = initVelocity * (-1 if wayLeft else 1)
	$Timeout.start(.25)
	pass

func _on_timeout_timeout():
	queue_free()
	pass # Replace with function body.
