extends CharacterBody2D

@export var ammo:PackedScene = preload("res://GameDVDCardtridge/Individenyous/Parts/bullet_invid.tscn")
@export var ammoArea:PackedScene = preload("res://GameDVDCardtridge/Individenyous/Parts/bullet_new_invid.tscn")
@export var yourColor:Color = Color.BLUE
@export var theirColor:Color = Color.RED
@export var myOpinion:Color = Color.CYAN
@export var theirOpinion:Color = Color.MAGENTA
@export var isPlayer:bool = true

const SPEED = 300.0
const JUMP_VELOCITY = -400.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var facingLeft:bool = false
var alignment:float = 1 # faction you're on. 1 is yours, -1 is opposition.

func _ready():
	$Form.self_modulate = yourColor if isPlayer else theirColor
	pass

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("Invid_Melompat") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("JalanKiri", "JalanKanan")
	if direction:
		velocity.x = direction * SPEED
		facingLeft = direction < 0
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
	
	
	
	# Shoot bullet
	if Input.is_action_pressed("Invid_Menembak"):
		if $DelayShoot.is_stopped():
			var parent = get_parent()
			var shot = ammoArea.instantiate()
			shot.set_position(self.position)
			shot.position.x += -1 if facingLeft else 1
			if shot.has_method('ownering'):
				shot.call('ownering',self)
				pass
			if shot.has_method('colorIt'):
				shot.call('colorIt',myOpinion if isPlayer else theirOpinion)
				pass
			if shot.has_method('shootWay'):
				#print('shooot')
				shot.call('shootWay',facingLeft)
				pass
			
			parent.add_child(shot)
			$DelayShoot.start(-1)
		pass
	
	move_and_slide()

func _on_delay_shoot_timeout():
	pass # Replace with function body.
