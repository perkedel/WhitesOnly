extends Area2D

@export var initVelocity:float = 1000
@export var wayLeft:bool = false

var velocitium:float = 0
var yourself:CharacterBody2D

# Called when the node enters the scene tree for the first time.
func _ready():
	#velocitium = initVelocity
	pass # Replace with function body.

func ownering(BelongsTo:CharacterBody2D):
	yourself = BelongsTo
	pass

func shootWay(toLeft:bool = false):
	wayLeft = toLeft
	#linear_velocity.x = initVelocity * (-1 if wayLeft else 1)
	velocitium = initVelocity
	$Timeout.start(.25)
	pass

func colorIt(what:Color = Color.MAGENTA):
	$Shape.self_modulate = what
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	position.x += delta * velocitium * (-1 if wayLeft else 1)
	pass

func _on_timeout_timeout():
	queue_free()
	pass # Replace with function body.


func _on_body_entered(body):
	if body == yourself:
		return
	#print('haa')
	pass # Replace with function body.
